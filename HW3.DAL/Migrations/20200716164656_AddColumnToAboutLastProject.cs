﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HW3.DAL.Migrations
{
    public partial class AddColumnToAboutLastProject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ShortestTaskId",
                table: "AboutLastProject",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AboutLastProject_ShortestTaskId",
                table: "AboutLastProject",
                column: "ShortestTaskId");

            migrationBuilder.AddForeignKey(
                name: "FK_AboutLastProject_TaskList_ShortestTaskId",
                table: "AboutLastProject",
                column: "ShortestTaskId",
                principalTable: "TaskList",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AboutLastProject_TaskList_ShortestTaskId",
                table: "AboutLastProject");

            migrationBuilder.DropIndex(
                name: "IX_AboutLastProject_ShortestTaskId",
                table: "AboutLastProject");

            migrationBuilder.DropColumn(
                name: "ShortestTaskId",
                table: "AboutLastProject");
        }
    }
}
