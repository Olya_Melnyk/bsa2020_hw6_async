﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HW3.DAL.Migrations
{
    public partial class DataAnnotations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProjectList_UserList_AuthorId",
                table: "ProjectList");

            migrationBuilder.DropForeignKey(
                name: "FK_ProjectList_TeamList_TeamId",
                table: "ProjectList");

            migrationBuilder.DropForeignKey(
                name: "FK_UserList_TeamList_TeamId",
                table: "UserList");

            migrationBuilder.DropIndex(
                name: "IX_UserList_TeamId",
                table: "UserList");

            migrationBuilder.DropIndex(
                name: "IX_ProjectList_AuthorId",
                table: "ProjectList");

            migrationBuilder.DropIndex(
                name: "IX_ProjectList_TeamId",
                table: "ProjectList");

            migrationBuilder.AlterColumn<string>(
                name: "LastName",
                table: "UserList",
                maxLength: 15,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "FirstName",
                table: "UserList",
                maxLength: 15,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "TeamList",
                maxLength: 15,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "TaskList",
                maxLength: 15,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "TaskList",
                maxLength: 30,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "ProjectList",
                maxLength: 20,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "ProjectList",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);
          
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.AlterColumn<string>(
                name: "LastName",
                table: "UserList",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 15);

            migrationBuilder.AlterColumn<string>(
                name: "FirstName",
                table: "UserList",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 15);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "TeamList",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 15);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "TaskList",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 15,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "TaskList",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 30);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "ProjectList",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 20);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "ProjectList",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));
        

            migrationBuilder.CreateIndex(
                name: "IX_UserList_TeamId",
                table: "UserList",
                column: "TeamId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectList_AuthorId",
                table: "ProjectList",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectList_TeamId",
                table: "ProjectList",
                column: "TeamId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectList_UserList_AuthorId",
                table: "ProjectList",
                column: "AuthorId",
                principalTable: "UserList",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectList_TeamList_TeamId",
                table: "ProjectList",
                column: "TeamId",
                principalTable: "TeamList",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserList_TeamList_TeamId",
                table: "UserList",
                column: "TeamId",
                principalTable: "TeamList",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
